import { check } from "express-validator";

function validateUser(method) {
  switch (method) {
    case "register": {
      return [
        check("name", "name is required")
          .exists({ checkFalsy: true })
          .isLength({ max: 50 })
          .withMessage("Name should be less than 50 characters"),

        check("email", "email is required")
          .exists()
          .isEmail()
          .withMessage("invalid email")
          .normalizeEmail(),

        check("password", "password is required")
          .exists({ checkFalsy: true })
          .isLength({ min: 5 })
          .withMessage("Password must be atleast 5 characters long"),
      ];
    }
  }
}

export default validateUser;
