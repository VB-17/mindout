import asyncHandler from "express-async-handler";
import { validationResult } from "express-validator";
import { hash, hashMatches } from "../utils/hashingUtil.js";
import User from "../models/userModel.js";

/**
 * @desc    Register a user
 * @route   POST /api/user/
 * @access  Public
 **/

export const registerUser = asyncHandler(async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  let { name, email, password } = req.body;
  let user = await User.findOne({ email });

  if (user) {
    res.status(400);
    throw new Error("User already exists");
  }

  const hashedPassword = await hash(password);

  user = await User.create({
    name,
    email,
    password: hashedPassword,
  });

  if (user) {
    res.json({
      _id: user._id,
      name,
      email,
    });
  } else {
    res.status(401);
    throw new Error("Invalid user data");
  }
});

/**
 * @desc    Login a user
 * @route   POST /api/user/login
 * @access  Public
 */

export const loginUser = asyncHandler(async (req, res) => {
  let { email, password } = req.body;

  const user = await User.findOne({ email });

  if (!user) {
    res.status(400);
    throw new Error("invalid email or password");
  }

  const isValidPassword = await hashMatches(password, user.password);

  if (!isValidPassword) {
    res.status(400);
    throw new Error("invalid email or password");
  }

  const token = user.generateAuthToken();
  res.json({ token });
});

