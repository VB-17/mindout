import asyncHandler from "express-async-handler";
import User from "../models/userModel.js";
import { validationResult } from "express-validator";

/**
 * @desc    Create a note
 * @route   PATCH /api/notes/
 * @access  Private
 */

export const createNote = asyncHandler(async (req, res) => {
  // const errors = validationResult(req);

  // if (!errors.isEmpty()) {
  //   return res.status(400).json({ errors: errors.array() });
  // }

  const data = req.body;

  const updatedNote = await User.findByIdAndUpdate(
    req.user._id,
    {
      $push: { notes: data },
    },
    { new: true }
  );

  if (updatedNote) {
    res.json(updatedNote);
  } else {
    throw new Error("An error occured while creating note");
  }
});

/**
 * @desc    Delete a note
 * @route   PATCH /api/notes/delete/:id?type="archive" => type field is optional
 * @access  Private
 */
export const deleteNote = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const { type } = req.query;
  let updatedNote;

  switch (type) {
    case "":
      updatedNote = await User.findOneAndUpdate(
        { _id: req.user._id, "notes._id": id },

        {
          $pull: {
            notes: { _id: id },
          },
        },
        {
          new: true,
        }
      );
      break;

    case "archive":
      updatedNote = await User.findOneAndUpdate(
        { _id: req.user._id, "notes._id": id },
        {
          $set: {
            "notes.$.isArchived": true,
          },
        },
        {
          new: true,
        }
      );
    default:
      throw new Erorr("Invalid query");
  }

  if (updatedNote) {
    res.json(updatedNote.notes.find((note) => note._id == id));
  } else {
    throw new Error("An error occured while deleting note");
  }
});

/**
 * @desc    Get a note
 * @route   POST /api/notes/:id
 * @access  Private
 */

export const getNoteById = asyncHandler(async (req, res) => {
  const { id: noteId } = req.params;
  const { _id: userId } = req.user;

  await User.find({ _id: userId, "notes._id": noteId }, (err, docs) => {
    if (docs) {
      const note = docs[0].notes.find((note) => note._id == noteId);
      res.json(note);
    } else if (err) {
      throw new Error("Error occured");
    }
  });
});

/**
 * @desc    Update a note
 * @route   PATCH /api/notes/update/:id
 * @access  Private
 */

export const updateNote = asyncHandler(async (req, res) => {
  const { id } = req.params;
  const { title, body } = req.body;

  const updatedNote = await User.findOneAndUpdate(
    { _id: req.user._id, "notes._id": id },
    {
      $set: {
        "notes.$.title": title,
        "notes.$.body": body,
      },
    },
    { new: true }
  );

  if (updatedNote) {
    res.json(updatedNote.notes.find((note) => note._id == id));
  } else {
    throw new Error("An error occured");
  }
});

export const getNotes = asyncHandler(async (req, res) => {
  let notes;
  const { type } = req.params;

  switch (type) {
    case "":
      notes = await User.find({ _id: req.user._id }).select("notes");
      break;

    case "archived":
      notes = await User.find({
        _id: req.user._id,
        "notes.isArchived": true,
      }).select("notes");
      break;

    case "bookmarked":
      notes = await User.find({
        _id: req.user._id,
        "notes.isBookmarked": true,
      }).select("notes");
      break;

    default:
      throw new Error("Invalid note type");
  }

  if (notes) {
    res.json(notes);
  } else {
    res.status(404);
    throw new Error("Cannot find notes by given params");
  }
});
