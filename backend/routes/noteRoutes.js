import express from "express";
import {
  createNote,
  deleteNote,
  getNoteById,
  getNotes,
  updateNote,
} from "../controllers/noteController.js";
import { protect } from "../middlewares/authMiddleware.js";

const router = express.Router();

router.get("/:type", [protect], getNotes);
router.patch("/", [protect], createNote);
router.get("/:id", [protect], getNoteById);
router.patch("/:id", [protect], deleteNote);
router.patch("/:id", [protect], updateNote);

export default router;
