import express from "express";
import { loginUser, registerUser } from "../controllers/userController.js";
import validateUser from "../validation/userValidation.js";

const router = express.Router();

router.post("/", [validateUser("register")], registerUser);
router.post("/login", loginUser);

export default router;
