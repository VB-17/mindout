import mongoose from "mongoose";
import jwt from "jsonwebtoken";

const noteSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },

    body: {
      type: String,
      required: true,
    },

    isBookmarked: {
      type: Boolean,
      required: true,
      default: false,
    },

    isArchived: {
      type: Boolean,
      required: true,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);

const userSchema = new mongoose.Schema(
  {
    name: { type: String, required: true, maxLength: 50, trim: true },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },

    notes: [noteSchema],

    password: {
      type: String,
      requried: true,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.methods.generateAuthToken = function () {
  return jwt.sign({ _id: this._id }, process.env.JWT_SECRET_KEY, {
    expiresIn: "30d",
  });
};

const User = mongoose.model("User", userSchema);
export default User;
